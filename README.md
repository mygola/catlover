You have to make an Android Native App with following Set of Instructions:


**Basic Rules:**

* Use 4.0 IceCreamSandwich as min SDK ie API-14.
* You can use Image Library and Network Library.
* Please make sure to handle Exceptions and Corner Cases like Internet Not Available Exception, Empty Checks  etc,
* Please sure to Make Proper Classes, Variable Names and try for optimised code you can write.
* You will be tested on your Coding Patterns / Completeness / Design and ability to write good code
* Screenshots Attached With it but Actual Ui might differ depending on requirements. (Please find it in the Source Folder).
* After Completion please send us the Zip file.

**App OverView:**

This is List-Detail Kind of App where on clicking on ListItem , user will be taken to Details Page.

The first page is:-

**LANDING PAGE**

* Title will be "CatLover".
* You have to use RecyclerView not ListView.
* The API-URL is [http://dev.mygola.com/cats](http://dev.mygola.com/cats)
* The Json provided by this URL is a list of Objects where every field is same except name is differentiated by "Cat" or "Dog".
* The Single item consists of Cat/Dog Random Image indicated as "picture" field and name as "name"
* If the name is  CAT then Image will be on Right and if the name is DOG then image will be on Left and same with the the name 
* On Click to any item of Recycler View , it will navigate to DetailsPage.


Other page is :-

**DETAILS PAGE:-**

* The Details Page is an Activity which shows all the Details of the CAT/DOG clicked on the Landing Page.
* Most of the Screen should be covered by the Cat/DOG "url" which is a picture url
* Below it "CAT/DOG" should be there in Big and Bold letters center aligned.
* Below it there is "eyeColor", "gender", "company" textFields aligned side by side.

Below it these is "about"
Below it there is "guid","email","phone" aligned side by side.

There is a "KILL the Cat/DOG" button which is fixed to bottom regardless of the Scrollsize.
On Click to "Kill the Cat/DOG" you should be taken back to MainActivity RecyclerView.
Handle orientation changes